# Météo TS

## Initialisation

Installation des dépendences :

```sh
npm i
```

Mise en production :

```sh
npm run build
npm start
```

Serveur de développement :

```sh
npm run dev
```

## Chemins API

`GET` /search/places?name=<nom>  
`POST` /places **(body: {geocodeApiPlaceId, name, latitude, longitude})**  
`GET` /places/weather  
`GET` /places/:id/forecast  
`GET` /forecast?latitude=<lat>&longitude=<lon>  
`DELETE` /places/:id

Pour utiliser **Postman**, voir le fichier `meteo_ts.postman_collection.json`

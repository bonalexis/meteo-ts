import { Request, Response } from "express";
import { Place } from "../Place";

export async function addFavorite(request: Request, response: Response) {
  const { geocodeApiPlaceId, name, latitude, longitude } = request.body;

  if (!geocodeApiPlaceId || !name || !latitude || !longitude) {
    return response.status(400).json({
      error:
        "You must supply a `name`, `latitude`, and `longitude` to add a favorite location.",
    });
  }

  try {
    const place = await Place.createNew(
      geocodeApiPlaceId,
      name,
      latitude,
      longitude
    );
    return response.json({ place });
  } catch (error) {
    console.error("Error while saving place to database:", error);
    return response.status(500).json({ error: "Internal server error" });
  }
}

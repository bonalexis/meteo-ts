import { Request, Response } from "express";
import { Place } from "../Place";

export async function deleteFavorite(request: Request, response: Response) {
  try {
    const place = await Place.findOne({
      where: { geocodeApiPlaceId: Number(request.params.id) },
    });

    if (!place) {
      return response.status(404).json({ error: "Place not found" });
    }
    await place.remove();
    return response.status(204).json({ message: "Place deleted" });
  } catch (error) {
    console.error("Error while deleting place:", error);
    return response.status(500).json({ error: "Internal server error" });
  }
}

import { Request, Response } from "express";
import { Place } from "../Place";
import { Weather } from "../Weather";

export async function getFavoriteWeather(request: Request, response: Response) {
  try {
    const places = await Place.find();
    if (!places) {
      return response.status(404).json({ error: "Place not found" });
    }

    const WeatherForPlaces: Weather[] = [];
    for (const place of places) {
      const weather = new Weather(place);
      await weather.setCurrent();
      WeatherForPlaces.push(weather);
    }

    return response.json(WeatherForPlaces);
  } catch (error) {
    console.error("Error while fetching weather data:", error);
    return response.status(500).json({ error: "Internal server error" });
  }
}

import { Request, Response } from "express";
import { Place } from "../Place";
import { Weather } from "../Weather";

export async function getWeatherById(request: Request, response: Response) {
  try {
    const place = await Place.findOne({
      where: { geocodeApiPlaceId: Number(request.params.id) },
    });

    if (!place) {
      return response.status(404).json({ error: "Place not found" });
    }
    const weather = new Weather(place);
    await weather.setCurrent();
    return response.json(weather);
  } catch (error) {
    console.error("Error while fetching weather data:", error);
    return response.status(500).json({ error: "Internal server error" });
  }
}

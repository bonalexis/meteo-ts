import { Request, Response } from "express";

export async function proximityWeather(request: Request, response: Response) {
  const { latitude, longitude } = request.query;

  if (!latitude || !longitude) {
    return response.status(400).json({
      error: "You must supply a `latitude` and `longitude` to get the weather.",
    });
  }

  try {
    const weatherResponse = await fetch(
      `https://api.open-meteo.com/v1/forecast?latitude=${latitude}&longitude=${longitude}&current=temperature_2m,weather_code`
    );
    const weather = (await weatherResponse.json()) as {
      current: { temperature_2m: number; weather_code: number };
    };

    return response.json(weather);
  } catch (error) {
    console.error("Error while fetching weather data:", error);
    return response.status(500).json({ error: "Internal server error" });
  }
}

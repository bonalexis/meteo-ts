import { Request, Response } from "express";

export async function searchPlaces(request: Request, response: Response) {
  const { name } = request.query;

  if (!name || Array.isArray(name)) {
    return response.status(400).json({
      error:
        "You must supply a single query param `name` to  search for locations.",
    });
  }

  try {
    const apiKey = "65a4fed00e84b807084661tisfc7f77";
    const apiUrl = `https://geocode.maps.co/search?q=${name}&api_key=${apiKey}`;
    const apiResponse = await fetch(apiUrl);

    const locations = await apiResponse.json();
    return response.json({ locations });
  } catch (error) {
    console.error("Error while fetching data from geocoding API:", error);
    return response.status(500).json({ error: "Internal server error" });
  }
}

import "reflect-metadata";
import express from "express";
import { DataSource } from "typeorm";
import { Place } from "./Place";
import { searchPlaces } from "./controllers/searchPlaces";
import { proximityWeather } from "./controllers/proximityWeather";
import { deleteFavorite } from "./controllers/deleteFavorite";
import { getWeatherById } from "./controllers/getWeatherById";
import { getFavoriteWeather } from "./controllers/getFavoriteWeather";
import { addFavorite } from "./controllers/addFavorite";

const dataSource = new DataSource({
  type: "sqlite",
  database: "./sqlite.db",
  entities: [Place],
  synchronize: true,
});

const PORT = 3500;
async function main() {
  await dataSource.initialize();
  console.log("Successfully connected to database.");
  const server = express();

  server.use(express.json());

  server.get("/", (_request, response) => {
    return response.json({ message: "Hello world!" });
  });

  server.get("/places/:id/forecast", getWeatherById);

  server.delete("/places/:id", deleteFavorite);

  server.get("/search/places", searchPlaces);

  server.get("/forecast", proximityWeather);

  server.get("/places/weather", getFavoriteWeather);

  server.post("/places", addFavorite);

  server.listen(PORT, () => {
    console.log(`Server listening on port ${PORT}.`);
  });
}

main();
